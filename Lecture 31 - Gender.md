# The Gendered Society
- [The Gendered Society](#the-gendered-society)
    - [Recall last lecture](#recall-last-lecture)
    - [Division of Labor](#division-of-labor)
    - [Distribution of Power](#distribution-of-power)
    - [The "second shift"](#the-%22second-shift%22)
    - [Distrubution of wealth](#distrubution-of-wealth)
    - [The STEM paradox](#the-stem-paradox)
    - [Gendered Careers](#gendered-careers)
---
## Recall last lecture
- **sex** : biological
- **gender** : social
- **stereotypes**: sec categories of "male" and "female", genders of "masculine" and "feminine".
- Today: How do sex and gender intersect with economic and political systems?

## Division of Labor
- In any society, not all people participate equally in all activities.  
    Different Tasks are assigned to different groups. Called **division of labor**
-   Sometimes the division is based on physical capabilities:
    -   *e.g* Strength to carry things
    -   *e.g* stamina to hunt antelopes
    -   *e.g* mammary glands to breastfeed an infant. (Rare situations that man can do this)
-   Tasks that could be done by a wide range of people are assigned to a group or category.  
    Based on age, social status, **gender**, etc.
-   The **gendered division of labor** look different in every culture, but there're general trends:
    -   Tasks requiring travelling or are dangerous are usually assigned to men.   
        *e.g* Several weeks away from home, etc.  
        Why: Female humans are the only physical capable to care infants, breastfeeding, etc. It's hard to move around. Don'tgo to hunt because don't want child to die.
-   *Note*: Societies often develop technoligies to obviate physical constraints.
    -   Pulley and pumbing systems, allow weaker people to get water uphill
    -   Vehicles, weapons allow convenient move and hunt.
    -   Breast Pumps and baby formula allow those w/o fully developed mammals to feed.
-   Not all women are limited by childbirth. (Some women have no children). Not all man are capable.
    -   Still same devision of labor?
    -   Some societies have more flexible divisions than others.
-   Primarily a cultural phenomenon, not the result of an inborn set of aptitudes.
    Evidenced with tremendous variance between societies.
-   Division of labor does not mean **gender stratification**.
    -   Gender Stratification arises when one group's contributions are considered more valuable.
    -   Foraging societies often lack gender stratification. Men and women's work are considered equally important.

## Distribution of Power
**Patriarchy**: A society in which men have more power than men.  
Lots of patriarchy societies, some societies do not have a gendered hierachy. But no **matriarchies**-> Why?
-   Men instinctive leaders and women instinctive followers? Probably not. Lots of couterexamples.
-   Physical constraints making power more accessile to men? Somwewhat, but lots of variance.   
    Restrictions in what a single gender can do. (Hunting is more powerful activity than cooking?)
-   Some patriarchal culture *diffused* around the world? A plausible explanation.

Patriarchy tend to go hand-in-hand with a **public-private distinction**.  
Industrialization and the emergence of market economies created a clear delineation between the **public** (realm of labor-for-pay) and the **private** (the realm of labor-for-love). There's a strong distinction between public and private. Only possible in industrialized societies.

In societies in whihc labor for pay is valorized, the private sphere is devalued. Institutions/ideologies continually reinforce the boundary separating public from private. In societies in which womens are allowed to work, travel through spaces... Still boundary. 50% of population are meant to stray at some time. 
- *e.g* Restriction against breastfeeding in public. Free to feed in home, censure if breastfeed in public. 
- *e.g* Street harassment, discourage female to spend time in public places.

In industralized, capitalist societies, the public qualities (stereotypically associated with men) tend to be seen as more valuable than private qualities (stereotypically associated with women). What these qualities are vary from culture to culture.  
Women in patriarchy societies are more toleranced. e.g. Tomboys. Harassment against women is more criminalized.  
Dominant ideologies generally serve to inforce power structures. Not to change power structures. Lots of scaffholding, instituions, etc.  
Although we have such technological advancements, we still have the same ideological system associating women with private sphere and men with the public space.

## The "second shift"
In marriages in the U.S., even when both spouses have full-time employment, women still spend more time each week on household tasks.  
Why do we still see women doing most of the household labor?
-   Boys and girls are encouraged to engage in different activities, pay attention to different things.  
    Boys and girls model their adult behavior after their own parents.  
-   Women are more likely to be blamed for a messy household.  
-   Men are stereotyped as innately bad at housework (think advertisements).    Women feel superior because they can?

These are difficult to change.

## Distrubution of wealth
All these lead to **feminization of poverty**: Women are increasingly more likely than men to live in poverty.   
Part of this is because women are more likely to be single heads of household. Even among single-parent households, those headed by women are more likely to be poor.

## The STEM paradox
Where would we expect the greatest women in STEM? Inversely linked to gender equality! Countries with less gender equality have larger percent women among STEM graduates.  
Reason: In countries with less gender equality, the STEM field is their only way to get power and wealth. In countries with more gender equality, women have many choices.

When Computer Science first emerged in the U.S., it was considered a feminine field, not prestigous, has low pay. *e.g* Typing is only taught in girl schools. - secretary
As it gained economic importance, the wage rose and shifted to men.

## Gendered Careers
Career choice is shaped by many different factors, and innate aptiture is only one of them.
-   How many societal paths to self-sufficienct are there>
-   How much are different vocations valued? (Money, prestige)
    *e.g.* NY street sanitizer earn as much as high school teachers.
-   How much are different categories of people valued.
-   Who is the workplace strutured for? What sorts of discrimination exist?  
    
Not only bioligical factors. Socital influences as well.
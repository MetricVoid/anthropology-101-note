# Power and Inqeuality

# What is culture
How do a certain set of beliefs, practises, etc. be acceptd by a group of people?

Does everyone in that group believe and engage in same practices? 

## What is ideology?
A typically negative word: Don't be ideological/He's an ideologue. Marx: "ideology" as oppressive.

Ideology is everywhere. Every viewpoint is ideological, no such thing as "unbiased" or "neutral" viewpoint.

## Cultural Hegemony
All perspectives are ideological. 
-   not all are treated equal ...
-   some have institutional support, ...
-   some accepted as "mainstream", "universal", "normal", ...

 **Hegemony**: A systen of domination where subjects comply seemingly voluntarily, rather than being forced. e.g. Armies

Example: Why do you follow laws? Why don't have more than one spouse? Why do/don't you wear heels?  
Stealing -> Stealing is wrong.(Self-policive. Internalized.)
We think we're freely choosing what to wear, although in a finite selection.   

## Hegemony
Hegemony is powerful, because they're *self-inforcing*. They do not to be reinforced.
-   They shape out perceptions of the world
    -   Same action can be preceived very differently, depending on who's the actor, who's the observer.
    -   Example: When is a suspect threatening enough for police to use deadly force?
### The police officer dilemma
Series of experiments conducted in UChicago.

Designed a first-person shooter:
-   "Suspects" (random people) appear in various settings.
-   You decide as quickly as you can whether they're carrying a gun or something.
-   If you think they carry a gun, press "shoot"
-   Get points for shooting, lose points for shooting falsefully.

Results:
-   The fastest decision are made when a black is holding a gun.
-   The slowest decisions are made when a black is not holding a gun. 
-   People are significantly more likely to shoot black innocents
-   and more likely to neglect white shooters.

Whether someone look threatening or innocent or not.

Hegemonic ideologies erase complexity and difference.
-   Confirmation bias: we notice things that fit our expectations, and things that don't fit our expectations are ignored. (e.g. Bad driving is often from woman.)
-   Or explained away as exceptions.
-   Or framed as jokes

Hegemonic ideologies shape our behavior in ways that seem to justify/confirm those ideologies.
-   Our perceptions lead us to behave in certain ways.
-   We comply with others' expectations to be recognizable. (e.g. wear commonly accepted clothes as feminine or matriline)

Hegemonic ideologies obscure their own partiality.
Unlike religious beliefs, which we all realize there're may religions. Hegemonic ideologies hide that fact
-   They seem to justify/expalin themselves
-   They seem common-sense and universal
-   Example: Race is a heremonic concept. The stereotypes associated with different racial categories are also hegemonic. It hide itself with psuedo-science or something.

## Prejudice and discrimination
**Prejudice**: negative atitudes toward people based on stereotypes about their category rather than actual knowledge about them.

**Discrimination**: Treating people unequally due to prejudice.
-   **de jure discrimination** policies that are discriminatory ont heir face (de jure = "by law")
-   **de facto discrimination** when policies are applied in discriminatory ways in practice. (de facto = "by fact", in actual practice)

Example: Voting laws in the U.S.

Before the 15th Amendment, states could have laws prohobiting black people from voting. (de jure)

The 15th Amendment made it unconsitutional to prevent people from their race. But states still engaged in *de facto* discrimination by applying non-racial policies unequally (*de facto*). Literacy tests are required to vote. These tests contain vague and weird questions, which can be judged right or wrong. The test is ot really about literacy, but selecting people to engage in voting.

## Can Hegemony be resisted?
Sure, or otherwise culture would never change.
But:
-   It's an uphill battle. Changing non-hegemony viewpoints doesn't
-   It requires collective action. One cannot single-handedly treat that.
-   Old hegemony systems generally need to be replaced with new hegemonic ideologies. (Sociality can't exist without a set of shared beliefs.)

## What is privilege?
To have privilege, is not to be affected by hardships that may affect others

Everyone has some privileges, and lacks other privileges.

Having privilege doesn't mean you don't work hard or that you didn't earn your accomplishments. It means other people might have to work even harder in order to achieve the same thing, due to prejudice ot lack of opportunity.

REcognizing our own privilege is not about feeling guilty, it's about how we judge others.
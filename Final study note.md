# ANTH 101 Final Exam Study Guide
- [ANTH 101 Final Exam Study Guide](#anth-101-final-exam-study-guide)
  - [Week 8](#week-8)
    - [Key terms](#key-terms)
      - [Enculturization](#enculturization)
      - [Cultural Relativism](#cultural-relativism)
      - [Ethnocentrism](#ethnocentrism)
      - [Emic vs. Etic](#emic-vs-etic)
    - [Key concepts](#key-concepts)
      - [Culture is learned, symbolic, integrated](#culture-is-learned-symbolic-integrated)
      - [No such things as cultureless humans](#no-such-things-as-cultureless-humans)
      - [Common Fieldwork Techniques: participant observation, interviews, genealogies, surveys, ethnography](#common-fieldwork-techniques-participant-observation-interviews-genealogies-surveys-ethnography)
  - [Week 9](#week-9)
    - [Key terms](#key-terms-1)
      - [Binary Oppositions](#binary-oppositions)
      - [Thick description](#thick-description)
      - [Consanguineal vs. affinial relations](#consanguineal-vs-affinial-relations)
      - [Nuclear family, family of orientation vs. family of precreation](#nuclear-family-family-of-orientation-vs-family-of-precreation)
      - [Descent groups; lineage vs. clan](#descent-groups-lineage-vs-clan)
      - [Unilineal descent (matrilineal descent, patrilineal descent) vs. bilateral descent](#unilineal-descent-matrilineal-descent-patrilineal-descent-vs-bilateral-descent)
      - [Cross cousins vs. parallel cousins](#cross-cousins-vs-parallel-cousins)
      - [Kin types vs. kin terms](#kin-types-vs-kin-terms)
      - [Cosmology](#cosmology)
      - [Collective effervescence](#collective-effervescence)
      - [Animism vs. polytheism vs. monotheism](#animism-vs-polytheism-vs-monotheism)
      - [Religious fundamentalism](#religious-fundamentalism)
      - [Religious revitalization](#religious-revitalization)
      - [Rites of passage and their stages (separation, liminality, reincorporaion)](#rites-of-passage-and-their-stages-separation-liminality-reincorporaion)
    - [Key concepts](#key-concepts-1)
      - [Be able to tell the difference between funcionalism, structural functionalst, and intepretive anthropological explanations](#be-able-to-tell-the-difference-between-funcionalism-structural-functionalst-and-intepretive-anthropological-explanations)
      - [Kinship is not the same as bioligical relatedness](#kinship-is-not-the-same-as-bioligical-relatedness)
      - [What rituals are and what they do](#what-rituals-are-and-what-they-do)
      - [How inversion rituals reinforce them](#how-inversion-rituals-reinforce-them)
  - [Week 10](#week-10)
    - [Key terms](#key-terms-2)
      - [Economy; subsistence economy vs. market economy](#economy-subsistence-economy-vs-market-economy)
      - [Foraging vs. food production (horticulture vs. agriculture vs. pastoralism)](#foraging-vs-food-production-horticulture-vs-agriculture-vs-pastoralism)
      - [Forms of exchange: market exchange, redistribution, reciprocity](#forms-of-exchange-market-exchange-redistribution-reciprocity)
      - [Forms of reciprocal exchange: generalized, balanced, negative](#forms-of-reciprocal-exchange-generalized-balanced-negative)
      - [Politics](#politics)
      - [Power vs. authority](#power-vs-authority)
      - [Medical anthropology](#medical-anthropology)
      - [Illness](#illness)
      - [Biomedicine](#biomedicine)
    - [Key concepts](#key-concepts-2)
      - [Exchange is not merely about objects but also about building and sustaining social relationships](#exchange-is-not-merely-about-objects-but-also-about-building-and-sustaining-social-relationships)
      - [Basic ways that political systems can differ: how power is distributed; how people attain positions of power; how non-powerful citizens can exert influence; which aspects of life fall under political control](#basic-ways-that-political-systems-can-differ-how-power-is-distributed-how-people-attain-positions-of-power-how-non-powerful-citizens-can-exert-influence-which-aspects-of-life-fall-under-political-control)
      - [The major ways in which medicine is a cultural phenomenon: wellness and (dis)ability are culturally defined, experience of illness is shaped by culture, diseases are transmitted and prevented through cultural practice; medical understandings are cultural](#the-major-ways-in-which-medicine-is-a-cultural-phenomenon-wellness-and-disability-are-culturally-defined-experience-of-illness-is-shaped-by-culture-diseases-are-transmitted-and-prevented-through-cultural-practice-medical-understandings-are-cultural)
      - [Childbirth is medicalized to different degrees in different cultures](#childbirth-is-medicalized-to-different-degrees-in-different-cultures)
  - [Week 11](#week-11)
    - [Key terms](#key-terms-3)
      - [Sex vs. gender](#sex-vs-gender)
      - [Intersex](#intersex)
      - [Stereotypes](#stereotypes)
      - [Transgender, cisgender](#transgender-cisgender)
      - [Supernumerary genders](#supernumerary-genders)
      - [Division of labor](#division-of-labor)
      - [Patriarchy vs. matriarchy](#patriarchy-vs-matriarchy)
      - [Marriage](#marriage)
      - [Incest prohibition](#incest-prohibition)
      - [Endogamy vs. exogamy](#endogamy-vs-exogamy)
      - [Polygamy; polyandry, polygyny](#polygamy-polyandry-polygyny)
      - [Levirate marriage, sororate marriage](#levirate-marriage-sororate-marriage)
      - [Patrilocality vs. matrilocality vs. neolocality](#patrilocality-vs-matrilocality-vs-neolocality)
    - [Key concepts](#key-concepts-3)
      - [Neither sex nor gender is a natural binary; both are cultural categories](#neither-sex-nor-gender-is-a-natural-binary-both-are-cultural-categories)
      - [Gender stereotypes vary from culture to culture](#gender-stereotypes-vary-from-culture-to-culture)
      - [Gendered division of labor is not the same as gender stratification](#gendered-division-of-labor-is-not-the-same-as-gender-stratification)
      - [How patriarchy and the public-private distinction often go together](#how-patriarchy-and-the-public-private-distinction-often-go-together)
      - [The feminization of poverty](#the-feminization-of-poverty)
      - [Incest prohibitions exist in all cultures but are defined differently in each culture](#incest-prohibitions-exist-in-all-cultures-but-are-defined-differently-in-each-culture)
      - [Cross-cousin marriage is common and can help maintain societal stability](#cross-cousin-marriage-is-common-and-can-help-maintain-societal-stability)
      - [Sexual behavior is not universally thought of as part of one’s identity](#sexual-behavior-is-not-universally-thought-of-as-part-of-ones-identity)
  - [Week 12](#week-12)
    - [Key terms](#key-terms-4)
      - [Ethnicity vs. race](#ethnicity-vs-race)
      - [Hypodescent](#hypodescent)
      - [Marked vs. unmarked categories](#marked-vs-unmarked-categories)
      - [Stereotype threat](#stereotype-threat)
      - [Hegemony](#hegemony)
      - [Prejudice vs. discrimination (de jure discrimination vs. de facto discrimination)](#prejudice-vs-discrimination-de-jure-discrimination-vs-de-facto-discrimination)
    - [Key concepts](#key-concepts-4)
      - [Why race is a social construct (and why that doesn’t mean that it doesn’t shape people’s lives)](#why-race-is-a-social-construct-and-why-that-doesnt-mean-that-it-doesnt-shape-peoples-lives)
      - [Hegemonic ideologies are self-reinforcing (they shape our perceptions; they erase complexity and difference; they influence our behavior; they frame themselves as common-sense and universal)](#hegemonic-ideologies-are-self-reinforcing-they-shape-our-perceptions-they-erase-complexity-and-difference-they-influence-our-behavior-they-frame-themselves-as-common-sense-and-universal)
  - [Week 13](#week-13)
    - [Key terms](#key-terms-5)
      - [Cultural imperialism](#cultural-imperialism)
      - [Westernization](#westernization)
      - [Acculturation](#acculturation)
      - [Indigenization](#indigenization)
      - [Cultural essentialism](#cultural-essentialism)
      - [Colonialism vs. imperialism](#colonialism-vs-imperialism)
      - [Indirect rule vs. direct rule vs. settler colonialism](#indirect-rule-vs-direct-rule-vs-settler-colonialism)
      - [Genocide, ethnocide](#genocide-ethnocide)
      - [Nation-state](#nation-state)
      - [Nationalism, ethnonationalism](#nationalism-ethnonationalism)
      - [Assimilation vs. pluralism](#assimilation-vs-pluralism)
      - [Multiculturalism](#multiculturalism)
    - [Key concepts](#key-concepts-5)
      - [How misunderstandings can arise as the result of different cultural backgrounds](#how-misunderstandings-can-arise-as-the-result-of-different-cultural-backgrounds)
      - [In situations of culture contact, the culture with the stronger military tends to overwhelm the other culture(s)](#in-situations-of-culture-contact-the-culture-with-the-stronger-military-tends-to-overwhelm-the-other-cultures)
      - [The essentialized picture of “Africa” in American media](#the-essentialized-picture-of-africa-in-american-media)
      - [Imperialism is motivated primarily by: profit, prestige, proselytizing](#imperialism-is-motivated-primarily-by-profit-prestige-proselytizing)
      - [Imperialist projects justify themselves by dehumanizing the people being conquered/displaced/killed](#imperialist-projects-justify-themselves-by-dehumanizing-the-people-being-conquereddisplacedkilled)
      - [The use of Native American imagery in mainstream American culture tends to draw on an essentialized, colonial-era stereotype of “the Indian”](#the-use-of-native-american-imagery-in-mainstream-american-culture-tends-to-draw-on-an-essentialized-colonial-era-stereotype-of-the-indian)
      - [“One state = one people = one language” is an ideal, not a description of how the world is actually structured](#one-state--one-people--one-language-is-an-ideal-not-a-description-of-how-the-world-is-actually-structured)
      - [Multiethnic countries differ with respect to: whether they favor assimilationism or pluralism/separatism; whether they encourage or discourage the public display of cultural difference](#multiethnic-countries-differ-with-respect-to-whether-they-favor-assimilationism-or-pluralismseparatism-whether-they-encourage-or-discourage-the-public-display-of-cultural-difference)
  - [Week 14](#week-14)
    - [Key terms](#key-terms-6)
      - [Prescriptivism vs. descriptivism](#prescriptivism-vs-descriptivism)
      - [Sapir-Whorf Hypothesis](#sapir-whorf-hypothesis)
      - [Language documentation](#language-documentation)
      - [Language revitalization](#language-revitalization)
    - [Key concepts](#key-concepts-6)
      - [Linguistic anthropologists study language in use](#linguistic-anthropologists-study-language-in-use)
      - [Different languages divide up the world differently](#different-languages-divide-up-the-world-differently)
      - [Different languages encode different aspects of experience](#different-languages-encode-different-aspects-of-experience)
      - [No language is inherently better than another, but languages are assigned value based on judgments of their stereotypical speakers](#no-language-is-inherently-better-than-another-but-languages-are-assigned-value-based-on-judgments-of-their-stereotypical-speakers)
      - [What AAE is](#what-aae-is)
      - [Why languages disappear](#why-languages-disappear)
  - [Week 15](#week-15)
    - [Key terms](#key-terms-7)
      - [Human Terrain System (HTS) project](#human-terrain-system-hts-project)
    - [Key concepts](#key-concepts-7)
      - [Know the general contours of the debates: studying “up” vs. “down”; studying the familiar vs. the strange; ethical dilemmas of HTS; anthropology as a science vs. anthropology as an art](#know-the-general-contours-of-the-debates-studying-up-vs-down-studying-the-familiar-vs-the-strange-ethical-dilemmas-of-hts-anthropology-as-a-science-vs-anthropology-as-an-art)

-----
## Week 8
### Key terms
#### _Enculturization_
Culture is not instinctive. We have to be *enculturated*.  
Humans are less dependent on instinct than any other animal, which enables tremendous flexibility and diversity.

Any person can be enculturated into any culture. Much of culture is learned *implicitly*, not through explicit instruction.
#### _Cultural Relativism_
The practice of a culture must be understood in their own cultural context. Every culture makes sense to its practitioners but may seem nonsensical, bizarre from an external perspective.

Cultural relativism is NOT the same as moral relativism. We can understand a cultural practice, but we do not necessarily approve of them.
#### _Ethnocentrism_
Opposite to relativism. Viewing other cultures through the lens of your own.
#### _Emic vs. Etic_
**Emic:** Local, native categories and perspectives.  
**Etic:** Distanced, objective perspective of the analyst.
In Anthropology fieldwork, anthropologists care more about emic views.
### Key concepts
#### _Culture is learned, symbolic, integrated_
**Learned:** Because culture is learned, it is cumulative. Each generation can pass information and knowledge to the next. They don't need to learn anew. Example: Technology can improve.

**Symbols:** Things representing things. Humans have the ability to understand a symbol as meaning something unrelated to that symbol. Language and a wide range of other human behaviors are symbolic.

**Integrated:** One aspect of culture is related to another, even if they may seem unconnected. Change in one domain can affect others. Example: Technology -> Art
#### _No such things as cultureless humans_
**Humans are inherently cultural beings.** Humans raised without culture lack certain characteristics considered human. Example: Not bipedal, underdeveloped brains, unable to communicate, etc.
#### _Common Fieldwork Techniques: participant observation, interviews, genealogies, surveys, ethnography_
**Participant Observation:** One of the best ways to learn about native experiences and understandings.  
**Interviews:** Can be short, one-time interviews, or more prolonged, repeated interviews. Can be about specific topics or entire life histories.  
**Genealogies:** A line of descent traced continuously from an ancestor. The study of tracing ancestors or development.  
**Surveys:** ...  
For all these techniques, **informed consent** is crucial. The culture/people being studied must know it.

-----

## Week 9
### Key terms
#### _Binary Oppositions_
Fundamental contrasts through which we understand the world. Example: Good vs. Bad, Life vs. Death, Sacred vs. Profane, ...

Theorized by Claude Levi-Strauss:  
There're basic cultural principles hard-coded into the brain (structures, hence *structuralism*). Different cultures flesh them out in different ways. And these "deep structures" take the form of **binary oppositions**.
#### _Thick description_
Explaining practices so that the reader can understand the native perspective. A thick description results from a scientific observation of any particular human behavior that describes not just the behavior, but its context as well, so that the behavior can be better understood by an outsider.
#### _Consanguineal vs. affinal relations_
**Consanguineal relations:** Genetically related.  
**Affinal relations:** Related by marriage.

Not all cultures consider affinal relations to be kin.
#### _Nuclear family, family of orientation vs. family of precreation_
Nuclear family: Small kin group consisting of parents and child(ren).

**Family of orientation:** The nuclear family in which one grows up. Example: Me and my parents.  
**Family of procreation:** The nuclear family one established once grown up. Example: Me, my partner, my children.

Common in two kinds of societies: Industrialized, forager.
#### _Descent groups; lineage vs. clan_
Descent group: Larger kin groups who share a common ancestor. Retain constant identity over time, even as membership changes.

**Lineage descent group:** A descent group that can trace their common descent back to a shared ancestor.  
**Clan descent group:** A descent group whose member claims a shared ancestor, but direct genealogical links aren't specified. The "shared ancestor" if a clan might even be a non-human totem.
#### _Unilineal descent (matrilineal descent, patrilineal descent) vs. bilateral descent_
**Unilineal:** Kinship is traced through either female relatives or male relatives.
- **Matrilineal descent:** Trace through mother's and daughters' descent group. Mother's sisters and brothers are included. Father is NOT included, but sons are included. Mother's brothers' children are not included. Brothers' children are not included. Sons' children are not included.
- **Patrilineal descent:** Trace through father's and son's descent group. Father's sisters and brothers are included. Mother is NOT included, but daughters are included. Father's sisters' children are not included. Sisters' children are not included. Daughters' children are not included.
- **Ambilineal Descent:** People choose which descent group they want to be a part of.

Weird rule: Spread out from ego, and stop whenever touching the first opposite gender, inclusive. But fathers are not included.

Opposite to unilineal descent is **bilateral**. All biological relations are considered kin.
#### _Cross cousins vs. parallel cousins_
**Cross cousins:** Father's sisters' kids or mother's brothers' kids. (Different gender.)  
**Parallel cousins:** Father's brothers' kids or mother's sisters' kids. (Same gender.)
#### _Kin types vs. kin terms_
**Kin types:** An etic view. An objective way of describing biological relatedness. Example: "Sister's daughter". "Mother's sister's son".  
**Kin terms:** An emic view. Culture- and language-specific ways of dividing up kin. 
#### _Cosmology_
A system for imagining and understanding the universe. Explains the world.
#### _Collective effervescence_
Periods of intense group solidarity. The whole group of people goes together, get excited, happy, etc.
#### _Animism vs. polytheism vs. monotheism_
Differences in what is worshiped:
- **Animism:** Divinity is located in natural beings/objects.
- **Polytheism:** Divinity is located in multiple supernatural beings.
- **Monotheism:** Divinity is located in a single supernatural being.
#### _Religious fundamentalism_
Movements within religions to return to an earlier, more "fundamental", usually stricter way of practicing the religion. Often goes hand-in-hand with *anti-modernism*. the rejection of modern technologies.
#### _Religious revitalization_
The renewal of religious behavior, usually during periods of societal change.
#### _Rites of passage and their stages (separation, liminality, reincorporation)_
The transition from one social group/status to another. 
- Separation: detach from the original group.
- Liminality: Stay in the middle. unclear.
- Reincorporation: Get into another group.

Example: Graduation, pledging a fraternity.
### Key concepts
#### _Be able to tell the difference between functionalism, structural functionalism, and interpretive anthropological explanations_
**Functionalism:**  Analyze the purpose of the practice. There're 7 basic human needs, and functionalists show how practices meet those (individual) needs.  
**Structural Functionalism:** How does this practice fulfill societal needs Societies need to maintain continuity and stability, despite constantly changing membership.  
**Interpretive anthropology:** What are people's motivations, and how do they interpret the practices. Focuses on *emic* rather than *etic*. Think on the point of view of the practitioner.  
#### _Kinship is not the same as biological relatedness_
Kinship has been around for much longer than our biological understanding of reproduction.
#### _What rituals are and what they do_
**Ritual:** *Formal, repetitive, stylized* process performed at special times and in special places. It reaffirms the values of a cultural group, usually through the use of *symbols*. Rituals ensure that people are fully integrated into a culture group.
#### _How inversion rituals reinforce them_
Some rituals *violate*, but not *reinforce*, community norms.  
Example: Native Australians annually kill and eat totemic animals, which is normally a sacred and forbidden taboo.  
Because these are *exceptional events*, they ultimately reinforce norms for everyday behavior.

-----

## Week 10
### Key terms
#### _Economy; subsistence economy vs. market economy_
**Economy:** A system for the production, distribution, and consumption of resources.  
**Subsistence economy:** People produce what they need to survive.  
**Market economy:** One where people trade goods/services for money and money/services for goods.

#### _Foraging vs. food production (horticulture vs. agriculture vs. pastoralism)_
**Foraging:** Persists in two types of areas: food production is extremely difficult(climate or terrain), or where food production is unnecessary(damp, temperate climates).  
Tend to be most egalitarian(equal social status)
#### _Forms of exchange: market exchange, redistribution, reciprocity_
**Market Exchange:** Money is exchanged for goods/services. Often involve bargaining to maximize.  
**Redistribution:** Resources are pooled and then given back out. Usually by the person in power.  
**Reciprocity:** Exchange of gifts between equals.
#### _Forms of reciprocal exchange: generalized, balanced, negative_
**Generalized reciprocity:** A gives B a gift, with no specific expectation of a return.  
**Balanced reciprocity:** A gives B a gift, expect B will give A an equivalent gift in return(at some point, maybe not immediately).  
**Negative reciprocity:** A gives B something, expect B to immediately give A something of equal or greater value. 
#### _Politics_
The ways in which power and authority are organized and exercised among groups.
#### _Power vs. authority_
**Power** is the ability to make people do things.  
**Authority** is a power that is respected and seen as deserved.
#### _Medical anthropology_
A subfield of cultural anthropology studies health and illness as *social* and *cultural* phenomena.

Medical practice is *social*: doctors and patients interact with each other.  
Medical practice is *cultural*: relies on culturally-specific beliefs and assumptions.
#### _Illness_
Individual's subjective(emic) experience of diseases.
#### _Biomedicine_
Diseases must be the result of specific, scientifically-proven biological processes, and must be treated with empirically-tested medical intervention.
### Key concepts
#### Exchange is not merely about objects but also about building and sustaining social relationships
Example: Kula ring. Armbands and necklaces are exchanged in a ring of islands. Everyone only possesses the items for a finite time, before passing them on again.
#### Basic ways that political systems can differ: how power is distributed; how people attain positions of power; how non-powerful citizens can exert influence; which aspects of life fall under political control
Power differentials:
- In Forager and horticultural societies, very little difference in how power is distributed.
- In chiefdoms and states, there're bigger differences in power.

Levels of hierarchy:
- Smaller societies = fewer levels of hierarchy
- Larger societies = more levels of hierarchy.

How do people become authority figures
- Inherited
- Acquired
  - Through unofficial community consensus
  - Through institutional processes(e.g. voting)
  - Through violence(e.g. or threat of violence)

How members exert influence on those in power
- Social approval/censure
- Threat of revolt
- Material rewards
- Material rewards
- Voting (Who can vote? Who does vote?)

Aspects of life under political control
- Legal systems
- Economic systems
- Access to resources
- Kinship
- Education
- Religion

#### The major ways in which medicine is a cultural phenomenon: wellness and (dis)ability are culturally defined, experience of illness is shaped by culture, diseases are transmitted and prevented through cultural practice; medical understandings are cultural
**Wellness:** What counts as sick/injury/mental illness. Example: "Homosexuality" is removed from APA diagnostic manual in 1973.  
**Disability:** Have difficulty accomplishing everyday tasks is labeled "disabled". But one's ability to accomplish "everyday tasks" is determined by the cultural environment.  
**Experience:** The same disease might be experienced as deliberating or merely inconvenient; terrifying or run-of-the-mill; humiliating or a source of pride.  
**Diseases transmit and prevention:** Figuring out what a disease is and how it spreads often involves understanding cultural practices.  
Example: mononucleosis is spread through cultural practices that involve saliva transfer.  
Washing hands, sneezing into elbows, using a condom, vaccinating... Cultural practices that developed to prevent the spread of disease.  
**Medical understandings:** Different cultures have different beliefs about how the human body works and what causes disease.
#### Childbirth is medicalized to different degrees in different cultures
In the U.S., childbirth is seen as a medical procedure, something that *happens* to people.  
In the Netherlands, childbirth is seen as an intimate personal project, something that people do.

-----

## Week 11
### Key terms
#### _Sex vs. gender_
**Sex** has to do with biology. **Gender** has to do with social rules.
#### _Intersex_
Exceptions to sexual dimorphism. (Sex in between).
#### _Stereotypes_
Sets of attributes that we assume go together.
#### _Transgender, cisgender_
**Transgender:** Having a gender identity that is different from the biological sex.  
**Cisgender:** Having a gender identity that matches the biological sex.
#### _Supernumerary genders_
Some cultures have more than two sex-gender categories. Those are called **supernumerary genders**
#### _Division of labor_
Different tasks are assigned to different people/groups.
#### _Patriarchy vs. matriarchy_
**Patriarchy:** Man have more power than women.  
**Matriarchy:** Women have more power than man.
#### _Marriage_
A type of non-consanguineal relationship between people that structures at least some of the following:
- Sexual activity
- Legal recognition
- Religious recognition
- Reproduction and childcaring
- Property ownership.
#### _Incest prohibition_
A societal taboo against marrying and/or having sex with your kin.
#### _Endogamy vs. exogamy_
**Exogamy:** Marrying outside your kin group.  
**Endogamy:** Marrying within your kin group.  
All marital practices are both endogamous(not too distant) and exogamous(not too close).
#### _Polygamy; polyandry, polygyny_
**Polygamy:** a person has more than one spouse
- **Polygyny:** more than one wife
- **Polyandry:** more than one husband
#### _Levirate marriage, sororate marriage_
**Levirate marriage:** A widowed women marry her deceased husband's brother.  
**Sororate marriage:** A widowed man marries his deceased wife's sister.  
Can help maintain kin alliances and keep property/children within a lineage.
#### _Patrilocality vs. matrilocality vs. neolocality_
**Partilocality:** Married couple moves to/near groom's father's house.
**Matrilocality:** Married couple moves to/near bride's mother's house.
**Neolocality:** Married couple forms new household in a new location.
### Key concepts
#### Neither sex nor gender is a natural binary; both are cultural categories
There is a spectrum from male to female. And we divide the spectrum of physiological difference up into discrete sex categories.
#### Gender stereotypes vary from culture to culture
Example: U.S. and Japan stereotypes of clothing. 
#### Gendered division of labor is not the same as gender stratification
Stratification arises when one group's contributions are considered more "important" or "valuable".  
For example, foraging societies have a gendered division of labor, but generally lack gender stratification: men's and women's contributions are seen as equally valuable.
#### How patriarchy and the public-private distinction often go together
Patriarchy goes hand-in-hand with **public-private distinction**. Industrialization caused **public** and **private** separation. Because labor-for-pay is valorized, so the private sphere is devalued.
#### The feminization of poverty
Globally, women are increasingly more likely than men to live in poverty.  Reason: Women are more likely than men to be single heads of household. As a job gain economic importance and prestige, wages rose and became associated with men.
#### Incest prohibitions exist in all cultures but are defined differently in each culture
Almost all societies have prohibitions of marrying and/or having sex with your kin. But, different societies define "kin" differently. So, different societies also have different incest prohibition.
#### Cross-cousin marriage is common and can help maintain societal stability
In either matrilineal or patrilineal descent system, *cross cousins* are never considered kin.  
Harmful recessive traits are slightly more likely to be manifested, but it also means the most harmful alleles are likely to be eliminated from the gene pool more quickly.  
Helps maintain stable ties between kin groups.
#### Sexual behavior is not universally thought of as part of one’s identity
In the contemporary U.S., sexual orientation is seen as an *identity*. Example: A man is seen as "gay" if he experiences attraction to other men, not women, regardless of his actual sexual behavior.

In many other societies, sexual behavior is seen as an *action*, not an aspect of identity. Example: Man may engage in sexual behavior with other men, but that is something he does, not something he is.

-----

## Week 12
### Key terms
#### _Ethnicity vs. race_
**Ethnicity:** based on shared cultural background (e.g. "African-American", "French", "Jewish")  
**Race:** based on assumed genetic similarities, usually as manifested in phenotype(e.g. "Black", "White", "Asian")
#### _Hypodescent_
Whiteness is considered "black", and any non-European heritage is taken to mean "non-white".
#### _Marked vs. unmarked categories_
**Unmarked category:** One that is perceived as the "default" or "neutral" state.  
**Marked category:** One that is perceived as "deviating" from the default/neutral state.
#### _Stereotype threat_
Knowing that you're expected to underperform actually causes you to underperform.
#### _Hegemony_
A system of domination(ideology) where subjects comply seemingly voluntarily, rather than being forced.
#### _Prejudice vs. discrimination (*de jure* discrimination vs. *de facto* discrimination)_
**Prejudice:** negative attitudes toward people based on stereotypes about their category, rather than actual knowledge about them.  
**Discrimination:** Treating people unequally due to prejudice.
- ***de jure* discrimination:** policies that are discriminatory on their face (de jure = by law)
- ***de facto* discrimination**: when policies are applied in discriminatory ways in practice (de facto = "by fact")
### Key concepts
#### Why race is a social construct (and why that doesn’t mean that it doesn’t shape people’s lives)
**Social construct:** a concept that is created by a society/culture, does not exist without a symbolic thought.

Racial categories are culturally determined. Which attributes are considered relevant to racial categorization are also culturally determined.
#### Hegemonic ideologies are self-reinforcing (they shape our perceptions; they erase complexity and difference; they influence our behavior; they frame themselves as common-sense and universal)
- Hegemony shape our perceptions
  - The same action can be perceived very differently depending on who the actor and observer are.
- Hegemonic ideologies erase complexity and difference
  - Confirmation bias: we notice things that fit our expectations, and things that don't fit our expectations are ignored, or explained as "exceptions", or framed as jokes.
- Hegemonic ideologies shape our behavior in ways that seem to justify/confirm those ideologies.
  - Our perceptions lead us to behave in certain ways.
  - In order to be recognizable, we need to comply with others' expectations.
  - Stereotype threat
- Hegemonic ideologies obscure their own partiality
  - Seem to justify themselves
  - Seem common-sense and universal
  - Example: Race

-----

## Week 13
### Key terms
#### _Cultural imperialism_
The spread of one culture at the expense of others; the imposition of one culture on another. Often includes:
- Exoticizing or "othering" the culture
- Framing the culture as "timeless" or ahistorical
- Involves dehumanizing the members of the culture
#### _Westernization_
the spread of "Western" culture, one example of cultural imperialism.
#### _Acculturation_
Cultural change as the result of contact between groups.  
Cultural imperialism is one example of acculturation.
#### _Indigenization_
The process by which borrowed cultural practices are modified to fit into a local culture.
#### _Cultural essentialism_
Reducing a complex, diverse culture to a simple set of stable, homogenous traits.
#### _Colonialism vs. imperialism_
**Colonialism:** long-term political and economic domination of a territory by a foreign power.  
**Imperialism:** expanding an empire through the seizure of foreign land.
#### _Indirect rule vs. direct rule vs. settler colonialism_
**Indirect rule:** Governing through native political structures and leaders. Example: British in India. Used local authorities to rule people.  
**Direct rule:** Impose new government led by members of the colonizing nation.  
**Settler Colonialism:** Colonizing nation sends large numbers of (non-politician) settlers, often displacing/killing native populations.
#### _Genocide, ethnocide_
**Genocide**: The deliberate killing of members of an ethnic group
- Can be done directly through murder, holocaust
- Can be done indirectly, like deliberate starvation
- May be used as a tool for imperialism
- May result from the ethnic conflicts sown by imperialist powers

**Ethnocide**: The deliberate elimination of a culture (w/o necessarily killing anyone)
- Example: "Residential" schools for Native Americans in the U.S. and Canada.
#### _Nation-state_
Nation: A group of people sharing a cultural or ethnic background (nationalities).  
State: A political unity and sovereignty  
So, **Nation-state**: a sovereign country that is associated with one ethnic or cultural group.
#### _Nationalism, ethnonationalism_
**Nationalism:** The view that one's own nation's interests or culture are more important than other nations'.  
**Ethnonationalism:** A form of nationalism where the nation is defined by ethnicity and/or race (as opposed to other forms of geographic or cultural difference)
#### _Assimilation vs. pluralism_
**Assimilation**: Cultural change where immigrant groups adopt the patterns/norms of the dominant"host" culture.  
**Pluralism:** The coexistence of multiple ethnic groups within one state, without the expectation of assimilation.
#### _Multiculturalism_
The view that cultural diversity in a country is good and desirable.
### Key concepts
#### How misunderstandings can arise as a result of different cultural backgrounds
People from different cultures have different beliefs, practices, norms, etc. When people from different groups interact, they may rely on conflicting ideologies and behavioral terms. Thus, misunderstanding can occur as a result.
#### In situations of culture contact, the culture with the stronger military tends to overwhelm the other culture(s)
Historically, the one with stronger military power determines which culture spreads.
#### The essentialized picture of “Africa” in American media
Acacia trees against a sunset.
#### Imperialism is motivated primarily by profit, prestige, proselytizing
- Profit: Controlling trade routes, natural resources, and cheap labor in the form of slavery.
- Prestige: The more territory claimed, the "stronger" or more "powerful" the nations are.
- Proselytizing: spreading a religion from personal salvation or to fulfill divine command.
#### Imperialist projects justify themselves by dehumanizing the people being conquered/displaced/killed
- Framing them as "subhuman" or "beastly", therefore their lives are not as valuable as European lives.
- Framing them as "natural slaves", in need of European guidance.
#### The use of Native American imagery in mainstream American culture tends to draw on an essentialized, colonial-era stereotype of “the Indian”
- What beliefs and attitudes lead to this pattern of use
  - Combining elements of many different cultures into one generic "Indian" culture reflects ignorance about/indifference to Native diversity.
  - Draws on essentialized stereotypes of Indians:
    - Men: warriors, brave, strong, violent(animal-like)
    - Women: exotic, sexy
    - Precisely the same stereotypes that were used by imperialist powers to justify killing/enslaving/abusing native populations
- Effects of this pattern used:
  - reinforcement of harmful stereotypes
  - Desecration of religious items
  - Continued erasure of the existence, diversity, and experiences of contemporary Native American people
- What should we do
  - Be aware of the cultural ideologies/stereotypes we're drawing on and reinscribing
  - Understand the historical baggage of colonialism and oppression that shapes current power dynamics
#### “One state = one people = one language” is an ideal, not a description of how the world is actually structured
The existence of *one* of these things can be used as a justification for the existence of the others.
#### Multiethnic countries differ with respect to whether they favor assimilationism or pluralism/separatism; whether they encourage or discourage the public display of cultural difference
Different countries take different approaches to govern their multiethnic populations: Assimilation vs. separatism.  
Encouraging vs. discouraging public displays of cultural difference.

-----

## Week 14
### Key terms
#### Prescriptivism vs. descriptivism
**Prescriptivism:** Approaches to language are ones that prescribe a "correct" way of using language. E.g. "I don't have nothing" is wrong.  
**Descriptivism:** Seeking to observe and describe actual language use. "Why they speak like that?"
#### Sapir-Whorf Hypothesis
Different languages produce different ways of talking.  
Grammatical categories of different languages lead their speakers to think about things in particular ways.  
This is about *habitual* thought patterns, not limits on what it's possible to think. You can still think about something even if you don't have a word for it.
#### Language documentation
Also known as "language preservation" or "salvage linguistics". Attempting to document a language before it goes extinct.  
Including writing dictionaries and grammars, making audio recordings, and documenting important stories/oral histories.

Difficulty:
- Takes considerable time and labor
- Require linguistic expertise
- Finding speakers can be difficult if extremely endangered.
#### Language revitalization
Not only documenting, but also finding more people to speak it. More likely to be done by people within the community.  
Successful example: Hebrew(was a dead language for more than a millennium before revitalization).

Difficulty:
- Endangered languages are often stigmatized, so young people don't want to speak it.
- Revitalization efforts often appeal to the "cultural identity" aspects of language, which doesn't encourage people to use it every day, use it in public, or encourage people from other cultural groups to learn.
### Key concepts
#### Linguistic anthropologists study language in use
- How do the ways we talk about the world reflect the ways we think about/perceive the world?
- How do different social groups use language differently?
- How do people use language strategically?
- What factors cause languages to disappear, and how can we save them?
#### Different languages divide up the world differently
Different languages may have different ways of expressing the same thing. For example, having more words for colors.
#### Different languages encode different aspects of experience
- **Time Tense:** In English, time tense is grammatically coded, but not in Chinese. It doesn't mean Chinese-speakers can't specify when something happened. Just what they aren't required to.  
- **Evidentiality:** Many languages have *grammatical evidentiality*: Whenever you state something, you have to indicate how you know it.
- **Positioning Terms:** Absolute terms(left/right), and absolute terms(north/south). English have both. But some languages have only absolute positioning terms. They may reproduce objects on a table with their absolute positioning.
#### No language is inherently better than another, but languages are assigned value based on judgments of their stereotypical speakers
All languages and dialects are equally systematic and effective for expressing what the speaker needs to express. But in practice, some ways of speaking are seen as prestigious, and other ways of speaking are stigmatized.  
Labov's study: In department stores for upper classes, SAE become more often.  
Example: AAE is used by African-Americans, a stigmatized group of speakers. So, many Americans consider it "broken". Standard American English is related to prestigious speakers, therefore selected as standard.
#### What AAE is
African-American English. Also called AAVE, a dialect to SAE(Standard American English). 
#### Why languages disappear
- Entire speaker community are wiped out(by natural disaster, disease, famine, genocide, etc.)
- More common: assimilation(voluntary or forced) into a more dominant/powerful culture - gradual language loss.
  - "Voluntary": Economic/Social/Communicative pressures.
  - Involuntary: Example: Native American children were taken away from parents and put into "boarding schools", texts destroyed.

-----

## Week 15
### Key terms
#### Human Terrain System (HTS) project
**HTS Project:** Teams of social scientists(anthropologists, sociologists, etc.) who are embedded with U.S troops and help them understand the local culture(s). Goal: Help the U.S troops take more effective action(forming relationships with local governments, providing aid to communities, "winning hearts and minds").
### Key concepts
#### Know the general contours of the debates: studying “up” vs. “down”; studying the familiar vs. the strange; ethical dilemmas of HTS; anthropology as a science vs. anthropology as an art
- **Studying up:** Studying people/cultures that are more powerful than themselves. **Studying down:** opposite to studying up?
  - Does studying up require different methodologies?
  - Does studying up require different ethical commitments?
- **Studying the familiar:** Most anthropologists study foreign cultures, which means western study the non-western. But increasingly, anthropologists are studying their own cultures.(Anthropological lens turned on Western cultures. People from non-western culture becoming anthropologists.)
  - Can they study their own culture?
  - Will it be more biased or less biased?
  - Is culture shock a benefit or boundary?
  - Do native anthropogists have more or less access?
  - What is one's "own culture"?
- **HTS Dilemma**:Should Anthropologists participate in HTS? American Anthropological Association: No.
  - HTS anthropologists are not in a position to obtain *voluntary informed consent*.
  - HTS anthropologists may be asked to endanger their informants.
  - HTS anthropologists may endanger other anthropologists or compromise their ability to conduct research.  
  But there're also anthropologists who believe that HTS is an opportunity to mitigate harm posed by the U.S military.
- **Science vs. Art:** Is anthropology an art?
  - Principles of empirical science: Reproducibility, Predictivity, Falsifiability, (Usually) comparative 
  - Position #1: Cultural Anthropology is a science, and it's good.
    - Observing cultural practices is ultimately not that different from observing people in a laboratory setting
    - It's possible to make objective observations of social facts
    - Anthropological analysis untimately rests on these verifiable observations
  - Position #2: Cultural Anthropology is not a science, and is bad.
    - No way to objectively record "facts" about culture
    - Not testable, not predictive
    - Motivated by political bias/advocacy
  - Position #3: Cultural Anthropology is not a science, and is good.
    - Clifford Geertz/Intepretive Anthropology: Anthropology is a process of qualitative interpretation.
    - Ethnographies are constructed, like histories or novels; there is an art to doing them well.
  - Position #4: Cultural Anthropology is not very much a science.
    - Hard sciences involve interpretation and assumption as well. Scientific "facts" are based on consensus - they're social.
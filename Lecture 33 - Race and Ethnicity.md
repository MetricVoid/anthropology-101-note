# Race and Ethnicity
- [Race and Ethnicity](#race-and-ethnicity)
    - [Three types of social differentiation](#three-types-of-social-differentiation)
    - [Race is a social construct](#race-is-a-social-construct)
    - [Race in genetics?](#race-in-genetics)
    - [Racial classification in the U.S.](#racial-classification-in-the-us)
        - [Marked v.s. Unmarked](#marked-vs-unmarked)
        - [Overlaps between ethnicity and race](#overlaps-between-ethnicity-and-race)
    - [How race matters](#how-race-matters)
        - [Explaining racial differences](#explaining-racial-differences)
        - [Aronson's research](#aronsons-research)
---
Race is really central to Anthropology. In responce to scientists who sort people into genetic groups -> races is mostly cultural, not genetic

## Three types of social differentiation
-   **Ethnicity**: Shared cultural background.  
    *e.g*:"African-American","French","Jewish"  
    The culture you grow up with.
-   **Race**:Physical appearance: Assumed genetic similarities, usually as manifested in phenotype.  
    *e.g*:"Black","White","Asian"
-   **Caste**: Same to race in lots of ways. Based on ancestry: who your parents are? Tied to occupations, usually stratified.  
    *e.g*:"untouchables" in India, *burakimin* in Japan.  
    There're still strong belief in such system.

In practice, theire is considerable overlap between one another. Some are considered very similar.

## Race is a social construct
-   **Social Constuct**: It is not natural in human mind, but a creation of society/culture.
    -   Racial categories are culturally determined.  
        They change in time, and they differ in countries. Demographic.
    -   Which attributs are considered relevant to racial categorization are culturally determined.
-   Racial categories are stereotypes: sets of attibuted that we assume go together. *e.g.*skin color, etc.

## Race in genetics?
Ancestry and genetics are important. People with ancestry from the same geographical areas tend to have more genetic similarity. Though, this is changing as prople are moving around the world more.  

We don't typically assign people to racial groups based on extensive DNA analysis. The way genes work means that different traits don't necessarily co-occur. The way we sort people into races are one of many ways, and they may not co-occur.   

Stereotype: These're the things that should go together. However, phenotypes does not link to ancestry.

## Racial classification in the U.S.
-   Pre civil-war era:
    -   Three categories: while, free blacks, slaves
    -   Determining someone's race was legally important: black people could not be citizens, could be enslaved.
    -   Each state set its own "threshold" for African ancestry, common one was: greater than 1/8 African ancestry = black
-   After civil-war:
    -   "Indian": and various East Asian categories were added to the census (Japanese, Chinese, ...)
    -   Still legally important for segregation laws (Black&White not use same facilities, not marry, etc.)
    -   Many states enacted one-drop rule: Any African heritage meant that a person was "colored" (thus subject to Jim Crow laws).
    -   Definition of "white" became more exclusionary after the abolotion of slavery. White tend to preserve more white rights, and to show their status.
-   Today, race is considered a matter of self-classification. 
    -   ~20 races for people to choose from. Can choose more than one.
    -   **Hypodescent** persists: whiteness is considered "blankness" and any non-European heritage is taken to mean "non-white".

Compared to Brazil, dozens of different racial categories arranged on a spectrum and based solely on phenotype, not presumed ancestry.

### Marked v.s. Unmarked
**Unmarked Category**: One that is perceived as "default" or "neutral" state.

**Marked Category**: One that is perceived as "deviating" from the default/neutral state.

"Cisgender" and "Trangender" are both gender identity categories, but "cisgender" is unmarked and "transgender" is marked.

"White", "Black", "Asian", "Latinx", "Native American" ,etc. are all racial categories. But white is unmarked and the rest are marked.

Ancestry is auctional. "What kind of American are you." etc. Commonly identified with ancestry to by hypothenical American.

### Overlaps between ethnicity and race
In the U.S., we often assume that racial categories corrspond neatly to ethnic groups (that we can tell someone's ethnicity based on their race)

Racial caterogies can become *ethnicized*(e.g. "Jewish" used be be a racial category, became an ethnic category.)

## How race matters
Race is a social construct, but it doesn't mean it doesn't shape people's lives.

More on Wednesday. Today: **stereotype threat**

### Explaining racial differences
Why do Euro-Am and Asian-Am tend to outperform and outnumber African-Am and Latino-Am academically?

Many explanations:
-   Socioeconomic: Wealth and money support
-   Cultural Differences: Culture value education differently
-   Low teacher expectations: Teacher treat students differently

These explanations all focus on individual skill as the reason for statisticall research.

### Aronson's research
Paradigm 1:
-   Double-blind setting. Subjects randomly assigned to contol or experimental groups.
-   One group is told that the test results have not historically shown ant gender/race/ethnic differences; the control group is not told anything.
-   Results: In the control group, racial/ethnic/gender differences. In the experimental group, equal performance.

Paradigm 2:
-   double-blind setting. Subjects randomly assigned to contol or experimental groups.
-   One group is told the test measures a specific, academic skill (implicity associated with gender/race); the other group is told the test examines problem-solving techniques(Not a stereotype related).
-   Group 1 unequal performace, group 2 equal performace.

Result: Knowing that you're expected to underperform actually causes you to underperform. Aronson's explanation: An awareness that stereotypes suggest you'll do badly is
-   distracting: undermines performance
-   discouraging: disengagement/disidentification (but do not deevaluation: I know Math is important, but I can't do good at it.)

and this phenomenon most strongly affects the people who care the most about the subject being tested.

# Marital and Sexual Practices
- [Marital and Sexual Practices](#marital-and-sexual-practices)
    - [Define marriage](#define-marriage)
    - [Incest Prohibitions](#incest-prohibitions)
    - [Cross-cousin marriage](#cross-cousin-marriage)
    - [Exogamy and Endogamy](#exogamy-and-endogamy)
    - [Polygamy](#polygamy)
    - [Second marriage preferences](#second-marriage-preferences)
    - [Residence Rules](#residence-rules)
    - [Marriage and Romantic Love](#marriage-and-romantic-love)
    - [Vary sexual practices](#vary-sexual-practices)
    - [Policing Sexual Behavior](#policing-sexual-behavior)
---
## Define marriage
Marriage is a type of (non-consanguineal) relationsgip between people that structures at lease some of the following:
-   sexual activity
-   romantic love
-   reproduction and childrearing (may not have children. may have child of others')
-   legal recognition (legal contract between)
-   religious recognition (religious ceremony/agreement)
-   property ownership (co-own property, woman own property, ..., pass down towards generation)  

In general, marriage include universal characters. How to marital practices vary cross-culturally?

## Incest Prohibitions
**Incest prohibition**: a societal taboo again marrying or/and having sex with your kin(specific category of person).  
All societies have incest prohibitions, but different societies define *kin* differently. (Kinship) different societies have different ways of recognizing if you're related to someone or not. This can vary a lot from culture to culture.

## Cross-cousin marriage
Recall from kinship:   
*Cross cousins* are children of different-sex siblings(Mother's brother, Father's sister). *Parallel Cousins* are the children of same-sex siblings(Father's brother, Mother's sister).  

*Unilineal* descent systems are ones in which kinship is only counted toward males or females.
-   Martilineal: Shared female ancestors
-   Patrilineal: Shared male ancestors

In any unilineal descent system, cross cousins are **never** considered kin.  
In many unilineal descent system, marrying cross cousins is not only permitted, but recommended. Harmful recessive traits are slightly more likely to be manifested, but it also means that harmful alleles are likely to be elimitated from the gene pool more quickly.  
Over time, cross-cousin marriage helps maintain stablity. Maintain kinship ties instead of moving kin groups.

## Exogamy and Endogamy
**exogamy**: marrying outside your group.
You cannot marry within your own group.
Helps create alliance between kin groups, not only individuals.  
**endogamy**: Marrying within your own group.  
Does not conflict each other. All martial practices are *both* endogamous and exogamous (not too distant, not too close).

## Polygamy
**Polygamy**: When a person has more than one spouse.
-   **Polygyny**: One male have more than one wife.
-   **Polyandry**: One female have more than one husband.  

Although polyamorous relationships exist in many cultures, they're only institutionally sanctioned in some. (Exist in US, not keep a form of marriage).  
In societies where polygamy is normative, it can lend added social and economic stability.  
Something inherently sexist about a man having multiple wifes. Becomes sexiest when women see men's property, resources in marriages... In patriarcheacal societies, women can have prestige after marriage. If there're multiple wifes that run one household, burden on women is lightened... If husband are mistreating them, women can band and be against him.

## Second marriage preferences
In many societies with lower lifespan expectancy  
**Levirate marriage**: Widowed woman marries deceased husband's brother  
**Sororate**: Widowed man marries deceased wife's sister.  
Help maintain kin alliances and keep property/children within a lineage. Recommended in some societies.

## Residence Rules
Where do a couple live after getting married?
-   **Partilocality**: Couple move to/near groom's father's house. More common in patrilineal societies, wife becomes husband's kin group.
-   **Matrilocality**: Married couple moves to/near bride's mother's house. More common in matrilineal societies.
-   **Avunculocality**: Moves to groom's mother's brother's house.
-   **Neolocality**: Moves to a new locations. May be near one or other parents, maybe outside the country.

## Marriage and Romantic Love
What makes people marry one another?  
In the U.S., "marry for love": people choose their own spouses. Not universal.  
Arranged marriages (set up by parents or other kin) are common in many societies (*e.g.* India). Some people choose future spouses when children is still young.  
Studies have shown that long-term happiness is approx. same for arranged and non-arranged marriages.

## Vary sexual practices
Different societies have different sexual standards. What sexual activities should be.  
In the U.S.:
-   **Heterosexual**: Sexually attracted to people of the opposite gender
-   **Homosexual**: Same gender
-   **Bisexual**: Attracted to men and women
-   **Pansexual**: Attracted to people reguardless of gender
-   **Asexual**: Not attracted to anyone.

In the contemporary U.S. society, these are IDENTITIES, not BEHAVIORS. A man is seen as gay if he experiences attraction to other men and not to women, regardless of his actual sexual behavior.  
In many other societies, sexual behavior is seen as an action, not an aspect of identity. *e.g.* A man may engage in sexual behavior with other men, but that's ACTIVITY, not something he *is*. He may not understood himself as gay.  
This is separate from stigmatization: Identity or behavior can be stigmatized. 

## Policing Sexual Behavior
All societies have restrictions on sexual behavior, but these restriction vary.  
Many societies have stricter restrictions on women's sexual behavior than on men's.
-   Women may be punished more for premarital sex and adultery.
-   Women may be considered "unmarriageable" if they have had sex with men.
-   FGM(Female Genital Mutilation) is practiced in some north African countries. Activities vary between contries. (Infibulation, circumcision, configulation). Ensures a women's virgin at marriage. Causes pain, infection, reduce sex pleasure, ...

Practice make sense in cultural relativism. The principle of cultural relativism does not say that we can't judge FGM to be wrong or try to stop it. (Same for male circumcision, child marriage, adult-child sexual activity, ...). It says, we first need to understand how and why people do it. Pragmatically speaking, interventions will be a lot more effective if they're informed by that understanding. Global intervention and change.
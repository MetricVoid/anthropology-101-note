# Cultures in Contact
What happens when cultures interact with one another? We can see wild things happening.
- [Cultures in Contact](#cultures-in-contact)
    - [What happens when cultures collide?](#what-happens-when-cultures-collide)
    - [Culture misunderstandings](#culture-misunderstandings)
        - [Cultural misunderstandings in LA](#cultural-misunderstandings-in-la)
    - [Culture Change](#culture-change)
    - [Cultural Essentialism](#cultural-essentialism)
    - [Essentialism and imperialism](#essentialism-and-imperialism)
    - 
## What happens when cultures collide?
People from very different cultures may encounter one another, through
-   Trade
-   Imperialism
-   War
-   Migration (voluntary or forced)
-   Tourism
-   Globalized media

What can happen in these situations?

E.g. reaction towards refugee is entirely different, because of difference in culture.

## Culture misunderstandings
People from different culture groups have different believes, practices, norms, etc. When they interact, they may rely on conflicting ideologies and behavioral norms.

Result: misunderstanding. Example: different ways of being polite/friendly. Some try not to offense, some go ragged.

### Cultural misunderstandings in LA
Benjamin Bailey’s research on Asian immigrant store owners and African-American customers in LA.
-   Store owners define politeness as avoiding imposition: minimal talk, focusing on transaction
-   Customers: Build camaraderie: small talk, personal questions.
-   Each interpret the other as being rude, without realizing that it’s cultural difference after all.

Example of what can happen when individuals with _equal power_ interact. However, if one have institutional/financial power over the other, we often see groups with unequal power interacting. E.g. trade, imperialism, war, migration, etc.

**Cultural Imperialism**: the spread of one culture at the expense of others, the imposition of one culture on another.

Historically, military power determines power among cultures.

## Culture Change
**Westernization**: the spread of “Western” culture is one example of cultural imperialism. Cultural Imperialism is one form of **acculturation**, cultural change as the result of contact between groups.

**Indigenization**: the process by which borrowed cultural practices are modified to fit into a local culture.
-   E.g.: McDonald’s have different menus in different countries. E.g. Indians do not eat beef. It have been indigenized.

## Cultural Essentialism
**Cultural Essentialism**: Reducing a complex, diverse culture to a simple set of stable, homogeneous traits. Often involves exoticizing or “othering” the culture, framing the culture as “timeless” or ahistorical, and dehumanizing the members of the culture.

E.g.: Representations of “the Middle East” as mysterious, mystical, “spicy”.
E.g.: Representations of “Africa” in American culture. Almost all books about Africa’s  cover is a tree under a setting sun.

“Do they know it’s Christmas?”: A song for raising fund, depicting Africa as starving, miserable place.

Africa culture in Disneyland is placed in Animal Kingdom, wilderness, where people are just animals. Other countries, cultures, etc, is architecture, people... Africa is riding animal, no buildings, only jungle as background.

Africa is portrayed as homogeneous, desolate, war-torn, impoverished, wild, jungle or desert. And many portrayals reinforce that impression. 

Pictures of African cities, etc. Does not fit into our common understanding, so was neglected. 

## Essentialism and imperialism
Any culture can be essentialized. E.g.: “American Parties” in Russia, Amsterdam, France: Guns, hamburgers, rants,...

When a more powerful group essentializes a less powerful group, this can be part of an overarching imperialist project.

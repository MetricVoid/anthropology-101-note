#Anthropological debates

## Debate 1: Is Anthropology a science?
The principles of empirical science:
- Reproducibility: You do the same experiment, and you get the same results.
- Predictivity: Not only what happens, but also predict how things will happen in the future.
- Falsifiability: If something can be disproven, then it's not scientific. (Falsifiable)
- (Usually) comparative: Compare between groups. (Experiment groups and control groups)

### The Napoleon Chagnon Controversy
In 1968, Chagnon published Yanomamo: *The Fierce People*. Argues that Yanomami(indegenous Amazonian culture) are "chronically violent", and this is the evidence that humans in general are naturally violent.

Many other anths objected to his work. Some objections were moral:
- Used by Venezuelan govt to justify discrimination against
- Engaged in unethical practices in order to gather data. Not informed about the reason. Contradicted their beliefs.

Other objections were based on Chagnon's work being bad science
- Used shoddy research practices that were neither unbiased nor removed. (e.g. He gave weapons as payment).
- Coming to conclusions about universal genetic hard-wiring based on the Yanomami is making a lot of logical leaps.

Therefore, his conclusion is invalid, because he's not doing science.

Chagnon and his supporters charactterized the criticism as a witch hunt. They accused other anths of being anti-science, and positioned themselves as the last remaining stalwarts of scientific anthropology.

Who are the real scientists? Were it science in the first place?

### Positions
Position #1: Cultural Anthropology is a science.
- Observing cultural practices is ultimately not that different from observing people in a laboratory setting
- It's possible to make objective observations of social facts
- Anthropological analysis untimately rests on these verifiable observations

Position #2: Cultural Anthropology is not a science, and is a bad thing.
- No way to objectively record "facts" about culture
- Not testable
- Not predictive
- Motivated by political bias/advocacy

Position #3: Cultural Anthropology is not a science, and is not a bad thing.
- Clifford Geertz/Intepretive Anthropology: Anthropology is a process of qualitative interpretation.
- Ethnographies are constructed, like histories or novels; there is an art to doing them well.

Position #4: Cultural Anthropology is not very much a science.
- Hard sciences involve interpretation and assumption as well. Scientific "facts" are based on consensus - they're social.

### Conclusion
Some cultural Anthropologists consider what they do to be a science, others do not. There're ways to do cultural anthropology that are more scientific and ways that are more humanistic. Scientific anthropology and humanistic anth both have valuable contributions to make

## Debate #2: Native Anthropology
Most cultural anthropologists study foreign cultures. traditionally, that has meant North American/European anthropologists studying non-Western cultures. But increasingly, anthropologists are studying their own cultures. 
- The anthropological lens is being turned to Western cultures.
- People from non-Western cultures are becoming anthropologists.

Can an anthropologist effectively study their own culture? Is the native anthropologist less biased or more biased? Is culture shock a benefit or a bounadry for the anthropogist? (Feeling upset/lonely/different). Do native anthropologists have more access or less access? (Will people tell you what they know? Will you ask about something considered norm?) What counts as "one's own culture?"

## Debate #3: Studying up
Even when cultural anthropologists turn to their own culture, they still tend to focus on the subaltern: the poor, the disadvantaged, the colonized, immigrant enclaves...

Is it possible for anthropologists to study people/cultures that are more powerful than themselves? 
- Does studying up require different methodologies? 
- Does studying up require different ethical commitments?

## Debate #4: Ethical dilemmas
In **applied anthropology** settings, where the anthropologist is doing research on behalf of an organization or company, to thom is the anthropologist's primary obligation?

If an anthropologist observeds illegal behavior, is he required to report? Or not to report?

Example: Nancy Scheper-Hughes's work on the global black market organ trade.
- Went "undercover" in places in hospotals, morgues, markets.
- Provided information to the FBI.
- Has been a harsh public critic of the medical establishment for its complicity in black market organ trading

Example: the Human Terrain System (HTS)
HTS Project teams of social scientists(anthropologists, sociologists, etc.) who are embedded with U.S troops and help them understand the local culture(s). Goal: Help the U.S troops take more effective action(forming relationships with local governments, providing aid to communities, "winning hearts and minds").

Should Anthropologists participate in HTS? American Anthropological Association: No.
- HTS anthropologists are not in a position to obtain *voluntary informed consent*.
- HTS anthropologists may be asked to endanger their informants.
- HTS anthropologists may endanger other anthropologists or compromise their ability to conduct research.
But there're also anthropologists who believe that HTS is an opportunity to mitigate harm posed by the U.S military. Getting the people satified, less death, "flip the switch"...

## Summary
Cultural Anthropology is a diverse field. There're many different ways to "do" anthropology. When working with human subjects, there're many complex ethical issues that arise, and anthropoligists have to wrestle against.
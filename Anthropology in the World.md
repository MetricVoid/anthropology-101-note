# Anthropology in the World

## Cultural Anthropology is diverse
Many different methodologies, theoretical approaches, different topics of study, many different political views about these differences. 

So what unites the field?

## Cultural relativism
Every culture is internally consistent and rational, and can only be fully understood from within.
- Corollary: Every culture at least a little bizzare from the outside!

Doesn't mean we can't compare ot make local judgement. Just we should be careful and try to be as informed as possible.

## Particularity and Generality
There're some human universals, though also tremendous difference. (Practices, laws, etc.)

All cultural anthropologists are tasked with the project of reconciling this binary:
- Where do we separate universal and specific(not assume too much. should we call them the same thing?)
- What form do the universals take? Cognitive structures, laws/patterns, needs?

## Translation
Ethnography is a process of translation, using a repertoire of universals to bridge cultural difference.

## Importance of individual experience
- How to people preceive and experience the world?
- How do they understand and explain what they're doing?
- How do the world shape their practices?

Look beyond initial impressions that mey be based on one's own cultural biases(those seem weird and bizzare). Beyond actors' own explanations for their behavior. Looking beyong the immediate moment, both forwards and backwards in time. Think human practice as a historical context: coming somewhere and going somewhere.

Why is anthropology important?
- Anthropologists help solve some of the most pressing issues of today. e.g.: climate change.
    - Study the practices that cause anthropogenic climate change(human acivity, understand human activity, which activities)
    - Assessing which communities are most affected by climate change and how to help them.
    - Recommending effective interventions to stop climate change
```
Example: Conservation movements
- Many non-profits working to stop the irreversible destruction of land through deforestation. mining, drilling, etc.
- NPs to protect "heritage sites"
- Help these NGOs coordinate with indigenous populations in the areas being protected.

Example: Medical Anthropology.
- Diagnosing, treating, and preventing diseases.
- Increasing population density and ease of travel means more epidemics. Anths study the practices that spread and prevent diseases. Recommend how to decrease the former and increase the latter.
- Increasing income inequality means increasingly divergent health outcomes. Work with poor communities and help goverment decide what their needs are, and what aid would be most helpful and effective.
```
Even if not looking to "change the world", A background in anthropology is useful for any career that involves interacting with/understanding people. Developing technology that works with people, not against them. 
- Developing tech with people

Having background in anthropology is useful for being a thoughtful and observant human being.
- Being aware of your own biases.
- Understanding before judging.
- Seeing how you are a part of complex social and cultural network.